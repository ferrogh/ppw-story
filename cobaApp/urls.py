from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='home'),
    path('bio', views.bio, name='bio'),
    path('form', views.form, name='form'),
    path('agenda', views.agenda, name='agenda'),
    path('deleteagenda', views.deleteagenda, name='deleteagenda'),
    path('adduser/', views.adduser, name='adduser'),
    path('validate/', views.validate, name='validate'),
    path('listuser/', views.listuser, name='listuser'),
    path('deleteuser/', views.deleteuser, name='deleteuser')
]