from django.db import models

# Create your models here.
class Agenda(models.Model):
    name = models.CharField(max_length=50)
    category = models.CharField(max_length=50)
    place = models.CharField(max_length=50)
    date = models.DateTimeField()

class User(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField(max_length=50)
    password = models.CharField(max_length=20)