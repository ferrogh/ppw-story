var iduser = 0;
var csrf_token = $("[name=csrfmiddlewaretoken]").val()

function list() {
    $.ajax({
        url: 'listuser/',
        success: function(result) {
            var users = result.users;
            var html = '';
            for (var i = 0; i < users.length; i++) {
                html += '<tr>';
                html += '<td>' + (i+1) + '</td>';
                html += '<td>' + users[i].name + '</td>';
                html += '<td>' + users[i].email + '</td>';
                html += '<td><button type="button" id="'+ users[i].id +'" class="ml-auto btn btn-danger unsubscribe" data-toggle="modal" data-target="#modal">Unsubscribe</button></td>';
                html += '</tr>';
            }
            $('tbody').html(html);
            $(".unsubscribe").click(function(){
                iduser = +$(this).attr('id');
                console.log(iduser);
            });
            
        }
    });
}

$(document).ready(function(){
    $("[name=email], [name=nama], [name=password]").on('input', function(){    
        $.ajax({
            url: "validate/",
            data: {
                "nama": $("[name=nama]").val(),
                "email": $("[name=email]").val(),
                "password": $("[name=password]").val()
            },
            method: "GET",
            success: function(result) {
                var status = result.data.status;
                var message = result.data.message;
                if (status == 'OK') {
                    $("button").removeAttr("disabled");
                    $("#emailHelp").html("Email is valid");
                    $("#passwordHelp").html("");
                    $("#namaHelp").html("");
                } else {
                    $("button").attr("disabled", true)
                    if (message.hasOwnProperty("email")) {
                        $("#emailHelp").html(message.email[0]);
                    } else {
                        $("#emailHelp").html("Email is valid");
                    }
                    if (message.hasOwnProperty("name")) {
                        $("#namaHelp").html(message.name[0]);
                    } else {
                        $("#namaHelp").html("");
                    }
                    if (message.hasOwnProperty("password")) {
                        $("#passwordHelp").html(message.password[0]);
                    } else {
                        $("#passwordHelp").html("");
                    }
                }
            }
        });
    });
    list();
});


$("#submit").click( function() {
    event.preventDefault();
    // console.log("masuk")
    $.ajax({
        url: 'adduser/',
        method: 'POST',
        data: {
            "csrfmiddlewaretoken": csrf_token,
            "nama": $("[name=nama]").val(),
            "email": $("[name=email]").val(),
            "password": $("[name=password]").val()
        },
        success: function(result) {
            // console.log(result);
            if (result.status == "OK") {
                alert("success");
                $("[name=nama]").val('');
                $("[name=email]").val('');
                $("[name=password]").val('');
                $("[name=nama]").trigger('input');
                list();

            } else {
                alert("fail");
            }
        }
    });
});

$('#modal').on('shown.bs.modal', function () {
    $('#pwddel').trigger('focus')
    var modal = $(this);
    $('#modalsubmit').click(function(){
        $.ajax({
            url: 'deleteuser/',
            method: 'POST',
            data: {
                "csrfmiddlewaretoken": csrf_token,
                "iduser": iduser,
                "password": $("#pwddel").val()
            },
            success: function(result) {
                // console.log(result);
                modal.modal('toggle');
                if (result.status == "OK") {
                    alert("success");
                    list();    
                } else {
                    alert("fail");
                }
            }
        })
    });
})