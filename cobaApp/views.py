from django.shortcuts import render
from .models import Agenda, User
from .forms import AgendaForm

# Create your views here.
from django.http import HttpResponseRedirect, JsonResponse

def index(request):
    response = { "home_active": "active"}
    return render(request, 'home.html', response)

def bio(request):
    response = { "about_active": "active"}
    return render(request, 'bio.html', response)

def form(request):
    response = { "reg_active": "active"}
    return render(request, 'form.html', response)

def agenda(request):
    form = AgendaForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid:
            form.save()
            return HttpResponseRedirect('agenda')
        else:
            return HttpResponseRedirect('/')
    response = {
        'agenda_active': "active",
        'agenda_form': AgendaForm,
        'agendas': Agenda.objects.all()
    }
    return render(request, 'agenda.html', response)

def deleteagenda(request):
    if request.method == 'POST':
        Agenda.objects.all().delete()
    return HttpResponseRedirect('agenda')

def adduser(request):
    if request.method == 'POST':
        user = User(name=request.POST.get('nama'), email=request.POST.get('email'), password=request.POST.get('password'))
        try:
            user.clean_fields()
            user.save()
            return JsonResponse({"status":"OK"})
        except Exception as e:
            return JsonResponse({"status":"Fail"})
    return HttpResponseRedirect('form')

def validate(request):
    status = {
        'status': 'OK',
        'message': 'Sukses'
    }

    email = request.GET.get('email')
    if User.objects.filter(email__iexact=email).exists() :
        status['status'] = 'Fail'
        status['message'] = {'email': ['Email is already registered']}
        return JsonResponse({"data": status})

    user = User(name=request.GET.get('nama'), email=request.GET.get('email'), password=request.GET.get('password'))
    try:
        user.clean_fields()
    except Exception as e:
        status['status'] = 'Fail'
        status['message'] = e.message_dict
    return JsonResponse({"data":status})

def listuser(request):
    users = list(User.objects.values('id', 'name', 'email'))
    return JsonResponse({'users': users})

def deleteuser(request):
    if request.method == 'POST':
        pas = request.POST.get('password')
        iduser = request.POST.get('iduser')
        user = User.objects.filter(id=iduser, password=pas)
        if user:
            user.delete()
            return JsonResponse({"status":"OK"})
        else:
            return JsonResponse({"status":"Fail"})
    return HttpResponseRedirect('form')