from django import forms
from . import models

class AgendaForm(forms.ModelForm):
    date = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type':'datetime-local'}), input_formats=['%Y-%m-%dT%H:%M',])
    class Meta:
        model = models.Agenda
        fields = ('name', 'category', 'place', 'date')
        labels = {
            'date': 'Date and Time'
        }
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })