from django.contrib import admin
from .models import Agenda, User
# Register your models here.

admin.site.register(Agenda)
admin.site.register(User)