from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from time import sleep
from .views import form


# Create your tests here.
class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(FunctionalTest, self).setUp()
        chrome_options = Options()

        # gitlab CI settings
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
    
    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_registration(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/form')

        email = selenium.find_element_by_id('email')
        name = selenium.find_element_by_id('nama')
        password = selenium.find_element_by_id('pwd')
        email_msg = selenium.find_element_by_id('emailHelp')
        name_msg = selenium.find_element_by_id('namaHelp')
        password_msg = selenium.find_element_by_id('passwordHelp')

        name.send_keys('Ferro')
        sleep(1)
        email.send_keys('ferro@ui.ac.id')
        sleep(1)
        password.send_keys('hehehe')
        sleep(1)
        submit = selenium.find_element_by_id('submit')
        submit.send_keys(Keys.RETURN)

        sleep(2)

        alert = selenium.switch_to.alert
        self.assertEqual(alert.text, 'success')
        alert.accept()
        sleep(1)
        self.assertEqual("", email.get_attribute('value'))
        self.assertEqual("", name.get_attribute('value'))
        self.assertEqual("", password.get_attribute('value'))

        self.assertEqual(name_msg.text, "This field cannot be blank.")
        self.assertEqual(password_msg.text, "This field cannot be blank.")
        self.assertEqual(email_msg.text, "This field cannot be blank.")
        
        name.send_keys('Ferro')
        sleep(1)
        email.send_keys('ferro@ui.ac.id')
        sleep(1)
        password.send_keys('hehehe')
        sleep(1)
        self.assertEqual(email_msg.text, "Email is already registered")

class PageTest(TestCase):
    def test_url_exist(self):
        response = Client().get('/form')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/bio')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/agenda')
        self.assertEqual(response.status_code, 200)
    
    def test_using_correct_template(self):
        response = Client().get('/form')
        self.assertTemplateUsed(response, 'form.html')
    
    def test_using_profile_func(self):
        found = resolve('/form')
        self.assertEqual(found.func, form)
    

