$(document).ready(function(){
    $("[name=email], [name=nama], [name=password]").on('input', function(){    
        $.ajax({
            url: "validate/",
            data: {
                "nama": $("[name=nama]").val(),
                "email": $("[name=email]").val(),
                "password": $("[name=password]").val()
            },
            method: "GET",
            success: function(result) {
                var status = result.data.status;
                var message = result.data.message;
                if (status == 'OK') {
                    $("button").removeAttr("disabled");
                    $("#emailHelp").html("Email is valid");
                    $("#passwordHelp").html("");
                    $("#namaHelp").html("");
                } else {
                    $("button").attr("disabled", true)
                    if (message.hasOwnProperty("email")) {
                        $("#emailHelp").html(message.email[0]);
                    } else {
                        $("#emailHelp").html("Email is valid");
                    }
                    if (message.hasOwnProperty("name")) {
                        $("#namaHelp").html(message.name[0]);
                    } else {
                        $("#namaHelp").html("");
                    }
                    if (message.hasOwnProperty("password")) {
                        $("#passwordHelp").html(message.password[0]);
                    } else {
                        $("#passwordHelp").html("");
                    }
                }
            }
        });
    });
});

$("button").click( function() {
    event.preventDefault();
    var csrf_token = $("[name=csrfmiddlewaretoken]").val()
    // console.log("masuk")
    $.ajax({
        url: 'adduser/',
        method: 'POST',
        data: {
            "csrfmiddlewaretoken": csrf_token,
            "nama": $("[name=nama]").val(),
            "email": $("[name=email]").val(),
            "password": $("[name=password]").val()
        },
        success: function(result) {
            // console.log(result);
            if (result.status == "OK") {
                alert("success");
                $("[name=nama]").val('');
                $("[name=email]").val('');
                $("[name=password]").val('');
                $("[name=nama]").trigger('input');

            } else {
                alert("fail");
            }
        }
    });
});